new Vue({
  el: '#app',
  data: {
    counter: 0,
    secondCounter: 0
  },
  computed: {
    output: function() {
      console.log('Computed');
      return this.counter > 5 ? 'больше 5' : 'меньше 5';
    }
  },
  watch: {
    counter: function(value) {
      let vm = this;
      setTimeout(function() {
        vm.counter = 0;
      }, 2000);
    }
  },
  methods: {
    result: function() {
      console.log('Method');
      return this.counter > 5 ? 'больше 5' : 'меньше 5';
    }
  }
});
