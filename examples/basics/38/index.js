new Vue({
  el: '#app',
  data: {
    users: [
      { name: 'John', email: 'jn@vmail.com', gender: 'male' },
      { name: 'Yuri', email: 'yuri@vmail.com', gender: 'male' },
      { name: 'Max', email: 'mx@vmail.com', gender: 'male' },
      { name: 'Victoria', email: 'vk@vmail.com', gender: 'female' },
      { name: 'Margo', email: 'mg@vmail.com', gender: 'female' }
    ]
  }
});
