new Vue({
  el: '#app',
  data: {
    color: 'gray',
    width: 100,
    height: 100
  },
  computed: {
    myStyle: function() {
      return {
        display: 'inline-block',
        backgroundColor: this.color,
        width: this.width + 'px',
        height: this.height + 'px',
        margin: '10px'
      };
    }
  }
});
